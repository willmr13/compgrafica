package views;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class MainWindow {

	private JFrame frmImageProgram;
	private JTextField filename;
	private boolean typefile = false;
	
	public static void showWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmImageProgram.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmImageProgram = new JFrame();
		frmImageProgram.setTitle("Image Program");
		frmImageProgram.setResizable(false);
		frmImageProgram.setBounds(100, 100, 450, 300);
		frmImageProgram.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmImageProgram.getContentPane().setLayout(new BorderLayout(0, 0));
		
		filename = new JTextField();
		filename.setEditable(false);
		filename.setText("");
		frmImageProgram.getContentPane().add(filename, BorderLayout.SOUTH);
		filename.setColumns(10);
		
		JMenuBar menuBar = new JMenuBar();
		frmImageProgram.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmOpenFile = new JMenuItem("Open File");
		mntmOpenFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser file = new JFileChooser(); 
		          file.setFileSelectionMode(JFileChooser.FILES_ONLY);
		          int i= file.showSaveDialog(null);
		        if (i==1){
		        	filename.setText("");
		        } else {
		            File arquivo = file.getSelectedFile();
		            filename.setText(arquivo.getPath());
		            String[] parts = filename.getText().split("\\.");
		            if (parts[1].equals("jpg") || parts[1].equals("png") || parts[1].equals("raw")) {
		            	typefile = true;
		            } else {
		            	JOptionPane.showMessageDialog(null,"Type Not Accepted.");
		            	filename.setText("");
		            }
		        }
			}
		});
		mnNewMenu.add(mntmOpenFile);
		
		JMenuItem mntmLoadFile = new JMenuItem("Load File");
		mntmLoadFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (filename.getText().equals("") ) {
					JOptionPane.showMessageDialog(null,"No File Open.");
				} else if(!typefile) {
					JOptionPane.showMessageDialog(null,"Type Not Accepted.");
				} else {
					String path = filename.getText();
					//JOptionPane.showMessageDialog(null,path);
					ShowImage img = new ShowImage(path);
					img.show(path);
				}
			}
		});
		mnNewMenu.add(mntmLoadFile);
		
		JMenu mnNewMenu_1 = new JMenu("Help");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mnNewMenu_1.add(mntmAbout);
	}

}
