package views;

import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import core.ImageFile;

public class Image extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public Image(ImageFile image) {
		int width = image.getWidth();
		int height = image.getHeight();
		String matrix[][] = image.getMatrix();
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
            	String[] cores = matrix[i][j].split(",");
            	Color color = new Color(Integer.parseInt(cores[0]),Integer.parseInt(cores[1]),Integer.parseInt(cores[2]),Integer.parseInt(cores[3]));
            	int colorVal = color.getRGB();
            	img.setRGB(i, j, colorVal );
            }
        }
        
        ImageIcon icon = new ImageIcon( img );
        add( new JLabel(icon) );
	}

}
