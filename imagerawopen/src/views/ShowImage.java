package views;

import java.awt.EventQueue;
import javax.swing.JFrame;
import core.ImageFile;

public class ShowImage extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String filename;
	
	public void show(String path) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowImage frame = new ShowImage(path);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});		
	}

	/**
	 * Create the frame.
	 */
	public ShowImage(String path) {
		filename = path;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		ImageFile image = new ImageFile(filename);
		add( new Image(image) );
        setLocationByPlatform( true );
        setResizable(false);
        pack();
	}

}
