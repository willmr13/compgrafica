package core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class ImageFile {
	
	private BufferedImage imagem;
	private int width = 0;
	private int height = 0;
	private String[][] matrix;
	
	public ImageFile(String filename) {
		try {
			imagem = ImageIO.read(new File(filename));
			width = imagem.getWidth();
			height = imagem.getHeight();
			matrix = new String[width][height];
			
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					int pixel = imagem.getRGB(i, j);
					int alpha = (pixel >> 24) & 0xff;
				    int red = (pixel >> 16) & 0xff;
				    int green = (pixel >> 8) & 0xff;
				    int blue = (pixel) & 0xff;
					matrix[i][j] = red+","+green+","+blue+","+alpha;
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error can't open file");
		}
	}

	public BufferedImage getImagem() {
		return imagem;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public String[][] getMatrix() {
		return matrix;
	}
	
}
