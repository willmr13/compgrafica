package principal;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

public class MostraPlanoBit extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	BufferedImage img,imagemTonsCinza;
	int bit;

	public MostraPlanoBit(int bit, BufferedImage imagemCinza) {
		super();
		this.imagemTonsCinza = imagemCinza;
		this.bit = bit;
		this.img = new BufferedImage(imagemTonsCinza.getWidth(), imagemTonsCinza.getHeight(), imagemTonsCinza.getType());
	}

	public void mostra() throws IOException{
		for( int i = 0; i < imagemTonsCinza.getWidth(); i++ ){ 
			for( int j = 0; j < imagemTonsCinza.getHeight(); j++ ){ 
				try{
					//Color newPixel = new Color(imagemAtual.getRGB(i, j) & 128);
					//img.setRGB(i, j, imagemAtual.getRGB(i, j) & 128);//gray recebe o valor do pixel
					//System.out.println(imagemCinza.getRGB(i, j) & 128);
					if((imagemTonsCinza.getRGB(i, j) & 1<<bit)==0){
						img.setRGB(i, j, Color.BLACK.getRGB());
					}else{
						img.setRGB(i, j, Color.WHITE.getRGB());
					}
				}catch (Exception e) { 
					e.printStackTrace();
				} 
			} 
		}
		if (img != null) {			 
            JFrame f = new JFrame("Fatiamento Por Plano de Bit " + (bit+1));
            JPanel contentPane;
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    		f.setBounds(100, 100, 450, 300);
            JMenuBar menuBar = new JMenuBar();
    		f.setJMenuBar(menuBar);
    		
    		JMenu mnNewMenu = new JMenu("Arquivo");
    		menuBar.add(mnNewMenu);
    		
    		contentPane = new JPanel();
    		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    		contentPane.setLayout(new BorderLayout(0, 0));
    		f.setContentPane(contentPane);
    		
            JLabel jlblHist = new JLabel();
            
            JScrollPane scrollPane = new JScrollPane();
    		contentPane.add(scrollPane, BorderLayout.CENTER);
    		scrollPane.setViewportView(jlblHist);
    		
            f.setSize(imagemTonsCinza.getWidth()+17, imagemTonsCinza.getHeight()+40);
			jlblHist.setIcon(new ImageIcon(img));
            f.getContentPane().add(jlblHist);
            f.setVisible(true);
            
            JMenuItem mntmNewMenuItem_1 = new JMenuItem("Salvar");
			mntmNewMenuItem_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					salvar();
				}
			});
			mnNewMenu.add(mntmNewMenuItem_1);
        } else {
            JOptionPane.showConfirmDialog(null, "Selecione uma op��o de imagem");
        }
	}
	
	public void salvar(){
		try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Salvar imagem...");
            FileFilter bmpFilter = new FileTypeFilter(".bmp", "Arquivos de Bitmap");
            FileFilter jpgFilter = new FileTypeFilter(".jpg", "JPEG");
            FileFilter pngFilter = new FileTypeFilter(".png", "PNG");
             
            fileChooser.addChoosableFileFilter(bmpFilter);
            fileChooser.addChoosableFileFilter(jpgFilter);
            fileChooser.addChoosableFileFilter(pngFilter);

            fileChooser.setMultiSelectionEnabled(false);
            fileChooser.setFileFilter(bmpFilter);
            fileChooser.setSelectedFile(new File("PlanoBit" + bit));
            FileFilter selectedFileType = fileChooser.getFileFilter();
            int opcao = fileChooser.showSaveDialog(null);
            
            if (opcao == JFileChooser.APPROVE_OPTION) {              
                
//                selectedFileType = bmpFilter;//default bmp
                
                File arquivo;
                
                if(selectedFileType.getDescription().equals(bmpFilter.getDescription())){
                	arquivo = new File(fileChooser.getSelectedFile() + ".bmp");
                	ImageIO.write(img, "bmp", new File(arquivo.getAbsolutePath() ));
                }
                else if(selectedFileType.getDescription().equals(jpgFilter.getDescription())){
                	arquivo = new File(fileChooser.getSelectedFile() + ".jpg");
                	ImageIO.write(img, "jpg", new File(arquivo.getAbsolutePath() ));
                }
                else if(selectedFileType.getDescription().equals(pngFilter.getDescription())){
                	arquivo = new File(fileChooser.getSelectedFile() + ".png");
                	ImageIO.write(img, "png", new File(arquivo.getAbsolutePath() ));
                }
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
	}
}
