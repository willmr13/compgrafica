package principal;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class Novo extends JFrame{
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage imagem;
	  private BufferedImage imagemCinza;
	  AreaImagem areaImagem;  

	  public Novo(){
	    super("Estudos Java");
	    
	    Container c = getContentPane();
	    c.setLayout(new BorderLayout());
		
	    JButton btn = new JButton("Carregar Imagem");
	    btn.addActionListener(
	      new ActionListener(){
	        public void actionPerformed(ActionEvent e){
	          JFileChooser fc = new JFileChooser();

	          int res = fc.showOpenDialog(null);
	          if(res == JFileChooser.APPROVE_OPTION){
		    File arquivo = fc.getSelectedFile();  
	          
	            imagem = null;
	          
	            try{
	              imagem = ImageIO.read(arquivo);
	            }
	            catch(IOException exc){
	              JOptionPane.showMessageDialog(null, 
	                "Erro ao carregar a imagem: " + 
	                exc.getMessage());
	            }

	            if(imagem != null){
	              areaImagem.imagem = imagem;
	              areaImagem.repaint();  
	            }
	          }
	        }
	      }
	    );

	    JButton btn2 = 
	      new JButton("Converter Escala Cinza");
	    btn2.addActionListener(
	      new ActionListener(){
	        public void actionPerformed(ActionEvent e){
	          converterEscalaCinza();
	        }
	      }
	    );

	    JPanel painel = new JPanel();
	    painel.setLayout(new FlowLayout());    
	    painel.add(btn);
	    painel.add(btn2);

	    c.add(painel, BorderLayout.SOUTH);
	    
	    // Cria a �rea de exibi��o da imagem
	    areaImagem = new AreaImagem();
	    c.add(areaImagem, BorderLayout.CENTER);    
		
	    setSize(400, 300);
	    setVisible(true);
	  }

	  public void converterEscalaCinza(){
	    ColorSpace cs = ColorSpace.getInstance(
	      ColorSpace.CS_GRAY);
	    ColorConvertOp op = new ColorConvertOp(cs, null);
	    imagemCinza = op.filter(imagem, null); 
	    areaImagem.imagem = imagemCinza;
	    areaImagem.repaint();
	  }
	  
	  public static void main(String args[]){
	    Novo app = new Novo();
	    app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  }
	}

	// Sub-classe de JPanel para exibir a imagem
	class AreaImagem extends JPanel{
	  /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	public BufferedImage imagem;

	  public void paintComponent(Graphics g){ 
	    super.paintComponent(g);
	    
	    // desenha a imagem no JPanel
	    g.drawImage(imagem, 0, 0, this);
	  } 
	}