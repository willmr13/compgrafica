package principal;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import com.google.common.io.Files;

public class View extends JFrame {
	static Processamento_de_imagem ObjProcessamento=new Processamento_de_imagem();
	JLabel lblNewLabel = new JLabel("");
	public Processamento_de_imagem getObjProcessamento() {
		return ObjProcessamento;
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View frame = new View();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public View() {
		setTitle("Fotoshops");
		

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Arquivo");
		menuBar.add(mnNewMenu);
		
		JMenu mnNewMenu_1 = new JMenu("Imagem");
		menuBar.add(mnNewMenu_1);
		JMenuItem mntmNewMenuItem = new JMenuItem("Abrir");
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Salvar");
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Converter em tons de cinza");
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Fatiamento por planos de bits");
		JMenuItem mntmHistogramaDeTons = new JMenuItem("Histograma de tons de cinza");
		
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		this.contentPane.add(scrollPane, BorderLayout.CENTER);
		scrollPane.setViewportView(lblNewLabel);
		
	
				
				mntmNewMenuItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						lblNewLabel.setIcon((Icon) new ImageIcon(ObjProcessamento.abrirImagen()));
						setBounds(100, 100, (lblNewLabel.getIcon().getIconWidth()+30), (lblNewLabel.getIcon().getIconHeight())+100);
						mntmNewMenuItem_1.setEnabled(true);
					}
				});
				
				
				mnNewMenu.add(mntmNewMenuItem);
				
				
				
				mntmNewMenuItem_1.setEnabled(false);
				mntmNewMenuItem_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ObjProcessamento.salvar();
					}
				});
				mnNewMenu.add(mntmNewMenuItem_1);
				

				
				
				mntmNewMenuItem_2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						lblNewLabel.setIcon(new ImageIcon(ObjProcessamento.escalaCinza())); 
					}
				});
				mnNewMenu_1.add(mntmNewMenuItem_2);
				
				
				mntmNewMenuItem_3.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						for(int i=0;i<8;i++){

								ObjProcessamento.fatiaCinza(i);
						}
					}
				});
				
				
				mntmHistogramaDeTons.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ObjProcessamento.contaCinza();
					}
				});
				mnNewMenu_1.add(mntmHistogramaDeTons);
				mnNewMenu_1.add(mntmNewMenuItem_3);
				
				JMenu mnFiltros = new JMenu("Filtros");
				menuBar.add(mnFiltros);
				
				JMenuItem mntmMdia = new JMenuItem("M\u00E9dia");
				mntmMdia.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						//chamada filtro m�dia
						lblNewLabel.setIcon(new ImageIcon(ObjProcessamento.media())); 
					}
				});
				mnFiltros.add(mntmMdia);
				
				JMenuItem mntmMediana = new JMenuItem("Mediana");
				mntmMediana.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//chamada filtro mediana
						lblNewLabel.setIcon(new ImageIcon(ObjProcessamento.mediana())); 
					}
				});
				mnFiltros.add(mntmMediana);
				
				JMenuItem mntmLaplaciana = new JMenuItem("Laplaciana");
				mntmLaplaciana.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//chamanda filtro laplace
						lblNewLabel.setIcon(new ImageIcon(ObjProcessamento.laplaciana())); 
					}
				});
				mnFiltros.add(mntmLaplaciana);
		
		
		
	}
	public void salvar(){

		try {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Salvar imagem...");
			FileFilter bmpFilter = new FileTypeFilter(".bmp", "Arquivos de Bitmap");
			FileFilter jpgFilter = new FileTypeFilter(".jpg", "JPEG");
			FileFilter pngFilter = new FileTypeFilter(".png", "PNG");

			fileChooser.addChoosableFileFilter(bmpFilter);
			fileChooser.addChoosableFileFilter(jpgFilter);
			fileChooser.addChoosableFileFilter(pngFilter);

			fileChooser.setMultiSelectionEnabled(false);

			fileChooser.setSelectedFile(new File(ObjProcessamento.getUltimoEndImagem().getName()));//usa o nome do ultimo arquivo aberto
			fileChooser.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent e) {
					// TODO Auto-generated method stub
					//if (JFileChooser.FILE_FILTER_CHANGED_PROPERTY.equals(e.getPropertyName())) {
						if(Files.getFileExtension(ObjProcessamento.getUltimoEndImagem().getName())=="bmp"){//checa a extens�o e usa o mesmo da imagem aberta
							fileChooser.setFileFilter(bmpFilter);
							fileChooser.setSelectedFile(new File((Files.getNameWithoutExtension(ObjProcessamento.getUltimoEndImagem().getAbsolutePath()))+".bmp"));
							System.out.println("bmp selecionado");
							fileChooser.repaint();
						}
						else if(Files.getFileExtension(ObjProcessamento.getUltimoEndImagem().getName())=="jpg"){
							fileChooser.setFileFilter(jpgFilter);
							fileChooser.setSelectedFile(new File((Files.getNameWithoutExtension(ObjProcessamento.getUltimoEndImagem().getAbsolutePath()))+".jpg"));
							fileChooser.repaint();
							System.out.println("jpg selecionado");
						}
						else if(Files.getFileExtension(ObjProcessamento.getUltimoEndImagem().getName())=="png"){
							fileChooser.setFileFilter(pngFilter);
							fileChooser.setSelectedFile(new File((Files.getNameWithoutExtension(ObjProcessamento.getUltimoEndImagem().getAbsolutePath()))+".png"));
							System.out.println("png selecionado");
						}
					//}
				}
			});


			//            fileChooser.setFileFilter(bmpFilter);
			FileFilter selectedFileType = fileChooser.getFileFilter();//filtra os dados a serem apresentados

			int opcao = fileChooser.showSaveDialog(null);

			if (opcao == JFileChooser.APPROVE_OPTION) {
				ImageIcon icone = (ImageIcon) lblNewLabel.getIcon();

				BufferedImage buffer = (BufferedImage) icone.getImage();

				if(selectedFileType.getDescription().equals(bmpFilter.getDescription())){
					File arquivo = new File(fileChooser.getSelectedFile() + ".bmp");
					ImageIO.write(buffer, "bmp", new File(arquivo.getAbsolutePath() ));
				}
				else if(selectedFileType.getDescription().equals(jpgFilter.getDescription())){
					File arquivo = new File(fileChooser.getSelectedFile() + ".jpg");
					ImageIO.write(buffer, "jpg", new File(arquivo.getAbsolutePath() ));
				}
				else if(selectedFileType.getDescription().equals(pngFilter.getDescription())){
					File arquivo = new File(fileChooser.getSelectedFile() + ".png");
					ImageIO.write(buffer, "png", new File(arquivo.getAbsolutePath() ));
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
