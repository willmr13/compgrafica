package principal;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FileChooserUI;
import javax.swing.plaf.basic.BasicFileChooserUI;


import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import com.google.common.io.Files;


public class Processamento_de_imagem {

	//Imagen atual carregada
	private static BufferedImage imagemAtual;
	private BufferedImage imagemCinza; 
	private File UltimoEndImagem;
	private static int altura;
	private static int largura;
	public File getUltimoEndImagem() {
		return UltimoEndImagem;
	}

	public void setUltimoEndImagem(File ultimoEndImagem) {
		UltimoEndImagem = ultimoEndImagem;
	}

	//Retorna um objeto BufferedImage
	public BufferedImage abrirImagen(){ 
		//Vari�vel de retorno
		BufferedImage bmp=null; 
		//Janela de sele��o de arquivo
		JFileChooser selector=new JFileChooser(); 
		//T�tulo da janela
		selector.setDialogTitle("Selecione uma imagem"); 
		//Filtro de arquivos
		FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG & GIF & BMP", "jpg", "gif", "bmp"); 
		selector.setFileFilter(filtroImagen); 
		//Abre a janela de sele��o
		int flag=selector.showOpenDialog(null); 
		//Se clicar em abrir
		if(flag==JFileChooser.APPROVE_OPTION){ 
			try { 
				//Pega o arquivo selecionado 
				UltimoEndImagem = selector.getSelectedFile(); 
				//atribuimos � vari�vel bmp
				bmp = ImageIO.read(UltimoEndImagem); 
			} catch (Exception e) { 
			} 

		} 
		//Atribuimos a imagem carregada para imageActual
		imagemAtual=bmp; 
		largura = imagemAtual.getWidth();
		altura = imagemAtual.getHeight();
		return bmp; 
	} 

	public BufferedImage escalaCinza(){ 
		try {
			imagemCinza = toGrayscale(imagemAtual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
			    //Graphics g = imagemCinza.getGraphics();
		imagemAtual=imagemCinza; 
		return imagemAtual;
		/*
		//Vari�veis de armazenamento de pixels 
		int mediaPixel,colorSRGB; 
		Color colorAux; 

		//Percoreendo pixel a pixel
		for( int i = 0; i < imageActual.getWidth(); i++ ){ 
			for( int j = 0; j < imageActual.getHeight(); j++ ){ 
				//Pega a cor do pixel
				colorAux = new Color(this.imageActual.getRGB(i, j)); 
				//Calcula a m�dia dos canais (red, green, blue) 
				mediaPixel = (int)((colorAux.getRed()+colorAux.getGreen()+colorAux.getBlue())/3); 
				//Muda para o formato sRGB 
				colorSRGB = (mediaPixel << 16) | (mediaPixel << 8) | mediaPixel; 
				//Asignamos el nuevo valor al BufferedImage 
				imageActual.setRGB(i, j,colorSRGB); 
			} 
		} 
		//Retornamos a imagen 
		return imageActual; */
		
	} 
	
	 private static BufferedImage toGrayscale(BufferedImage image)  
	            throws IOException {  
	        BufferedImage output = new BufferedImage(image.getWidth(),  
	                image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);  
	        Graphics2D g2d = output.createGraphics();  
	        g2d.drawImage(image, 0, 0, null);  
	        g2d.dispose();  
	        return output;  
	    }  
	
	public void contaCinza(){
		int[] valores = new int[256];//vetor para armazenar a quantidade de bits
	    DefaultCategoryDataset dataset = new DefaultCategoryDataset();//dataset usado para o JFreeChart
		for( int i = 0; i < imagemAtual.getWidth(); i++ ){ 
			for( int j = 0; j < imagemAtual.getHeight(); j++ ){ 
				try{
					int gray = imagemAtual.getRGB(i, j) & 0xFF;//gray recebe o valor do pixel
					valores[gray]++;//o respectivo nivel recebe + 1
				}catch (Exception e) { 
				} 
			} 
		}
		for (int x = 0; x < valores.length; x++) {
            String v2 = String.valueOf(x);//adiciona legenda em x
            dataset.addValue(valores[x], "Cinza", v2);//adiciona os valores no dataset

        }
		JFreeChart chart = ChartFactory.createLineChart(
                "Histograma", // chart title
                "Pixels", // domain axis label
                "Quantidade", // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                true, // include legend
                true, // tooltips?
                false // URLs?
                );
		
		BufferedImage img = imagemAtual;
		if (img != null) {			 
            final ChartPanel panel = new ChartPanel(chart);
            JFrame f = new JFrame("Histograma");
            JLabel jlblHist = new JLabel();

            f.setSize(800, 600);
            try {
                ChartUtilities.saveChartAsJPEG(new File("histograma.JPG"), chart, 800, 600);
                img = ImageIO.read(new File("histograma.JPG"));
                jlblHist.setIcon(new ImageIcon(img));
            } catch (IOException ex) {
                
            }
            f.getContentPane().add(panel);
            f.setVisible(true);
        } else {
            JOptionPane.showConfirmDialog(null, "Selecione uma op��o de imagem");
        }

	}
	
	public void fatiaCinza(int bit){
		if(imagemCinza==null){
			escalaCinza();
		}
		MostraPlanoBit fatia = new MostraPlanoBit(bit, imagemCinza);
		try {
			fatia.mostra();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void salvar(){

		try {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Salvar imagem...");
			FileFilter bmpFilter = new FileTypeFilter(".bmp", "Arquivos de Bitmap");
			FileFilter jpgFilter = new FileTypeFilter(".jpg", "JPEG");
			FileFilter pngFilter = new FileTypeFilter(".png", "PNG");
			
			//adicionando filtros 
			fileChooser.addChoosableFileFilter(bmpFilter);
			fileChooser.addChoosableFileFilter(jpgFilter);
			fileChooser.addChoosableFileFilter(pngFilter);

			//desabilitando multisele��o
			fileChooser.setMultiSelectionEnabled(false);

			//adiciona um ouvinte para a��es do filechooser
			fileChooser.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent e) {
					File arquivoSelecionado;
					System.out.println("Ouvinte do fileChooser acionado!");
						if(fileChooser.getFileFilter()==bmpFilter){//checa se o filtro de tipo � bmp
							try {
						        FileChooserUI fcUi = fileChooser.getUI();//pega o User Interface
						        Class<? extends FileChooserUI> fcClass = fcUi.getClass();//pegando uma classe do filechooserUI
						        Method setFileName = fcClass.getMethod("setFileName", String.class);//m�todo da fcClass resgatado
						        BasicFileChooserUI basicUi=(BasicFileChooserUI)fcUi;//pegando o BasicFileChooserUI do fcUi
						        String nomeInserido = basicUi.getFileName();//pega o valor do nome de arquivo
						        if(nomeInserido.isEmpty())//se n�o tiver nada inserido
						        	arquivoSelecionado = UltimoEndImagem;//usa o mesmo nome do arquivo aberto
						        else arquivoSelecionado=(new File(basicUi.getFileName()));//sen�o usa o nome inserido
						        setFileName.invoke(fcUi, (Files.getNameWithoutExtension(arquivoSelecionado.getName()))+ ".bmp");//insere no UI o nome mais a exten��o

						    } catch (Exception e1) {
						        e1.printStackTrace();
						    }

							System.out.println("bmp selecionado");
						}
						else if(fileChooser.getFileFilter()==jpgFilter){
							try {
						        FileChooserUI fcUi = fileChooser.getUI();
						        Class<? extends FileChooserUI> fcClass = fcUi.getClass();
						        Method setFileName = fcClass.getMethod("setFileName", String.class);
						        BasicFileChooserUI basicUi=(BasicFileChooserUI)fcUi;
						        String nomeInserido = basicUi.getFileName();						        
						        if(nomeInserido==null)
						        	arquivoSelecionado = UltimoEndImagem;
						        else arquivoSelecionado=(new File(basicUi.getFileName()));
						        setFileName.invoke(fcUi, (Files.getNameWithoutExtension(arquivoSelecionado.getName()))+ ".jpg");

						    } catch (Exception e1) {
						        e1.printStackTrace();
						    }
							System.out.println("jpg selecionado");
						}
						else if(fileChooser.getFileFilter()==pngFilter){
							try {
						        FileChooserUI fcUi = fileChooser.getUI();
						        Class<? extends FileChooserUI> fcClass = fcUi.getClass();
						        Method setFileName = fcClass.getMethod("setFileName", String.class);
						        BasicFileChooserUI basicUi=(BasicFileChooserUI)fcUi;
						        String nomeInserido = basicUi.getFileName();						        
						        if(nomeInserido==null)
						        	arquivoSelecionado = UltimoEndImagem;
						        else arquivoSelecionado=(new File(basicUi.getFileName()));
						        setFileName.invoke(fcUi, (Files.getNameWithoutExtension(arquivoSelecionado.getName()))+ ".png");

						    } catch (Exception e1) {
						        e1.printStackTrace();
						    }
							System.out.println("png selecionado");
						}
				}
			});


			int opcao = fileChooser.showSaveDialog(null);

			if (opcao == JFileChooser.APPROVE_OPTION) {//ao clicar em salvar
				File arquivo = fileChooser.getSelectedFile();//pega caminho do arquivo a ser salvo
				System.out.println("clique em salvar");
				if(fileChooser.getFileFilter().equals(bmpFilter)){//se for selecionado para salvar em bmp
					if(Files.getFileExtension(arquivo.getName()).isEmpty())//verifica se a extens�o est� vazia
						ImageIO.write(imagemAtual, "bmp", new File(arquivo.getAbsolutePath()+".bmp" ));//se estiver vazia adiciona
					else
						ImageIO.write(imagemAtual, "bmp", new File(arquivo.getAbsolutePath() ));//sen�o salva como est�

				}
				else if(fileChooser.getFileFilter().equals(jpgFilter)){
					if(Files.getFileExtension(arquivo.getName()).isEmpty())
						ImageIO.write(imagemAtual, "jpg", new File(arquivo.getAbsolutePath()+".jpg" ));
					else
						ImageIO.write(imagemAtual, "jpg", new File(arquivo.getAbsolutePath() ));
				}
				else if(fileChooser.getFileFilter().equals(pngFilter.getDescription())){
					if(Files.getFileExtension(arquivo.getName()).isEmpty())
						ImageIO.write(imagemAtual, "png", new File(arquivo.getAbsolutePath()+".png" ));
					else
						ImageIO.write(imagemAtual, "png", new File(arquivo.getAbsolutePath() ));
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catOch block
			e1.printStackTrace();
		}
	}
	
	public BufferedImage media() {
		int vizinhanca = 0;
		
		String str = (JOptionPane.showInputDialog("Insira a constante"));//showConfirmDialog(null, "Selecione um o valor de vizinhan�a");
		if(!str.isEmpty()){
			vizinhanca = Integer.parseInt(str);
		}
		if(vizinhanca < 1){
			JOptionPane.showMessageDialog(null, "Valor inv�lido! Ser� utilizado o valor padr�o : 10.");
			vizinhanca = 10;
		}
		BufferedImage imagem = imagemAtual;
		Integer altura = imagem.getHeight();
		Integer largura = imagem.getWidth();
		int[][][] matrizImagem = new int[altura][largura][3];
		for (int j = 0; j < altura; j++) {
			for (int i = 0; i < largura; i++) {
				int rgb = imagem.getRGB(i, j);
				matrizImagem[j][i][0] = (rgb >> 16) & 255;
				matrizImagem[j][i][1] = (rgb >> 8) & 255;
				matrizImagem[j][i][2] = (rgb) & 255;
			}
		}
		imagem = mediaVizinhanca(vizinhanca, imagemAtual, matrizImagem);
		//BufferedImage resultado = new BufferedImage(imagemAtual.getWidth(),imagemAtual.getHeight(),imagemAtual.getType());
		return imagem;
	}

	public static BufferedImage mediaVizinhanca(int vizinhanca, BufferedImage imagemOriginal, int[][][] matrizImagem) {
		Integer altura = imagemOriginal.getHeight();
		Integer largura = imagemOriginal.getWidth();
		int[][][] matrizNovaImagem = new int[altura][largura][3];
		int somaR = 0;
		int somaG = 0;
		int somaB = 0;
		int contador = 0;
		int limiteLateral = vizinhanca / 2;
		int limSuplinha, limInflinha, limSupColuna, limInfColuna;
		for (int j = 0; j < altura; j++) {
			for (int i = 0; i < largura; i++) {
				limInflinha = j - limiteLateral;
				limSuplinha = j + limiteLateral;
				limInfColuna = i - limiteLateral;
				limSupColuna = i + limiteLateral;
				for (; limInflinha <= limSuplinha; limInflinha++, limInfColuna = i - limiteLateral) {
					for (; limInfColuna <= limSupColuna; limInfColuna++) {
						if ((limInflinha >= 0) && (limInfColuna >= 0) && (limInflinha < j) && (limInfColuna < i)) {
							somaR += matrizImagem[limInflinha][limInfColuna][0];
							somaG += matrizImagem[limInflinha][limInfColuna][1];
							somaB += matrizImagem[limInflinha][limInfColuna][2];
							contador++;
						}
					}
				}
				if (somaR != 0) {
					somaR = (int) Math.round(somaR / contador);
				}
				if (somaG != 0) {
					somaG = (int) Math.round(somaG / contador);
				}
				if (somaB != 0) {
					somaB = (int) Math.round(somaB / contador);
				}
				matrizNovaImagem[j][i][0] = somaR;
				matrizNovaImagem[j][i][1] = somaG;
				matrizNovaImagem[j][i][2] = somaB;
				somaR = somaG = somaB = contador = 0;
			}
		}
		BufferedImage novaImagem = new BufferedImage(largura, altura, imagemOriginal.getType());
		for (int j = 0; j < altura; j++) {
			for (int i = 0; i < largura; i++) {
				int r = ((matrizNovaImagem[j][i][0])) & 255;
				int g = ((matrizNovaImagem[j][i][1])) & 255;
				int b = (matrizNovaImagem[j][i][2]) & 255;
				int rgb = (r << 16) | (g << 8) | (b);
				novaImagem.setRGB(i, j, rgb);
			}
		}
		return novaImagem;
	}
	
	public BufferedImage mediana() {
		// Cria imagem de saida com mesmo tamanho e tipo da imagem de entrada

		BufferedImage ima_out  = new BufferedImage(imagemAtual.getWidth(),imagemAtual.getHeight(),imagemAtual.getType());

		// Recupera matriz das imagens de entrada e saida

		Raster raster = imagemAtual.getRaster(); // declara e instancia objeto raster soh para leitura
		WritableRaster wraster = ima_out.getRaster(); // declara e instancia objeto raster para escrita

		// Processa valores da imagem de entrada e armazena na imagem de saida

		double valornr, valorng, valornb;
		int[] v = new int[9];

		for(int y=1; y<imagemAtual.getHeight()-1; y++)
			for(int x=1; x<imagemAtual.getWidth()-1; x++){
				//            Aplica Filtro de Mediana 3x3

				LeJanela3x3(raster,v,x,y,0);
				valornr = CalcMediana(9,v);

				LeJanela3x3(raster,v,x,y,1);
				valorng = CalcMediana(9,v);

				LeJanela3x3(raster,v,x,y,2);
				valornb = CalcMediana(9,v);

				wraster.setSample(x,y,0,(int)(valornr+.5));
				wraster.setSample(x,y,1,(int)(valorng+.5));
				wraster.setSample(x,y,2,(int)(valornb+.5));

			}

		return ima_out;
	}
	// M�todo para Ler uma janela 3x3 de uma banda de uma imagem de entrada num vetor v.
	// (x,y) representa a coluna e a linha central da janela

	public static void LeJanela3x3(Raster raster, int []v, int x, int y, int banda){
		v[0] = raster.getSample(x-1,y-1,banda);
		v[1] = raster.getSample(x  ,y-1,banda);
		v[2] = raster.getSample(x+1,y-1,banda);
		v[3] = raster.getSample(x-1,y  ,banda);
		v[4] = raster.getSample(x  ,y  ,banda);
		v[5] = raster.getSample(x+1,y  ,banda);
		v[6] = raster.getSample(x-1,y+1,banda);
		v[7] = raster.getSample(x  ,y+1,banda);
		v[8] = raster.getSample(x+1,y+1,banda);

		return;
	}
	// M�todo para C�lculo da Mediana de um vetor de npts pontos

	public static double CalcMediana(int npts, int []v){
		int aux;
		// Ordena em ordem crescente os elementos do vetor

		for(int i=0; i<npts-1; i++)
			for(int j=i+1; j<npts; j++)
				if(v[i] > v[j]){
					aux = v[i]; v[i]=v[j]; v[j]=aux;
				}

		// Define o valor da mediana
		if((npts%2)==0)
			return((double)v[npts/2]);
		else
			return((double)((v[npts/2]+v[npts/2+1])/2.));
	}
	public BufferedImage laplaciana() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat mat	= img2Mat(imagemAtual);
		
		//FuncImage.bufferedImageShow(mat,"Original");
		// method 1
		Imgproc.Laplacian(mat, mat,-1);
		
		//method 2
		/*int kernelSize = 3;
		Mat kernel = new Mat(kernelSize, kernelSize, CvType.CV_32F){
			{
				put(0, 0, 0);
				put(0, 1, -1);
				put(0, 2, 0);
				put(1, 0, -1);
				put(1, 1, 4);
				put(1, 2, -1);
				put(2, 0, 0);
				put(2, 1, -1);
				put(2, 2, 0);
			}
		};
		Imgproc.filter2D(mat, mat, -1, kernel);*/
		return mat2BufferedImage(mat);
		
		//FuncImage.BufferedImageShow(mat, "Laplacian");
	}
	
	public static Mat img2Mat(BufferedImage in){
          Mat out;
          byte[] data;
          int r, g, b;

          if(in.getType() == BufferedImage.TYPE_INT_RGB){
              out = new Mat(largura, altura, CvType.CV_8UC3);
              data = new byte[largura * altura * (int)out.elemSize()];
              int[] dataBuff = in.getRGB(0, 0, largura, altura, null, 0, largura);
              for(int i = 0; i < dataBuff.length; i++){
                  data[i*3] = (byte) ((dataBuff[i] >> 16) & 0xFF);
                  data[i*3 + 1] = (byte) ((dataBuff[i] >> 8) & 0xFF);
                  data[i*3 + 2] = (byte) ((dataBuff[i] >> 0) & 0xFF);
              }
          }
          else{
              out = new Mat(altura, largura, CvType.CV_8UC1);
              data = new byte[largura * altura * (int)out.elemSize()];
              int[] dataBuff = in.getRGB(0, 0, largura, altura, null, 0, largura);
              for(int i = 0; i < dataBuff.length; i++){
                r = (byte) ((dataBuff[i] >> 16) & 0xFF);
                g = (byte) ((dataBuff[i] >> 8) & 0xFF);
                b = (byte) ((dataBuff[i] >> 0) & 0xFF);
                data[i] = (byte)((0.21 * r) + (0.71 * g) + (0.07 * b)); //luminosity
              }
           }
           out.put(0, 0, data);
           return out;
     } 
	
	public static BufferedImage mat2BufferedImage(Mat mat ){
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if(mat.channels() > 1)
			type= BufferedImage.TYPE_3BYTE_BGR;
		int bufferSize = mat.channels() * mat.cols() * mat.rows();
		byte[] b = new byte[bufferSize];
		mat.get(0, 0,b);
		BufferedImage bufferedImage = new BufferedImage(mat.cols(),mat.rows(),type);
		final byte[] targetPixels = ((DataBufferByte)bufferedImage.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return bufferedImage;
	}

}
